<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Http\Resources\CartResource;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cart = Cart::orderBy('id', 'desc')->get();
        return CartResource::collection($cart);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $cart = Cart::where('nama', request('nama'))->first();

        if ($cart) {
            $cart->update([
                'jumlah' => $cart->jumlah + request('jumlah')
            ]);
        } else {
            $c = Cart::create([
                'nama' => request('nama'),
                'jumlah' => request('jumlah')
            ]);

            return new CartResource($c);
        }

        return new CartResource($cart);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart = Cart::findOrFail($id);

        $cart->update([
            'jumlah' => request('jumlah')
        ]);

        return new CartResource($cart);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::destroy($id);

        return 'Cart Deleted';
    }
}
